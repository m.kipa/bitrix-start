require('webpack');
let path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const autoprefixer = require('autoprefixer');

// Устанавливаем дополнительные пути статики
// =========================================
const nodePath = path.join('node_modules/');
const customNodePath = path.join('local/static/modules/');
const customNodePathBuild = path.join('local/static/bundle/');

const sourceMaps = process.env.MODE === 'development' ? 'cheap-eval-source-map' : false;
const sourceMapsCss = process.env.MODE === 'development';

module.paths.unshift(customNodePath);
module.paths.unshift(customNodePathBuild);

const miniCss = new MiniCssExtractPlugin({
    filename: 'css/[name].css',
});

// Стартовый конфиг
// ===============
let config = {
    mode: 'none',
    entry: {
        // пример entry - меняем их на свои.
        project: './local/static/assets/sass/global/project.sass',
        projectJs: './local/static/assets/js/global/project.js',
    },
    output: {
        path: path.resolve(__dirname, 'local/static/bundle'),
        filename: '[name].js',
        publicPath: '/local/static/bundle/',
    },
    module: {
        rules: [
            // JSX
            // ===
            {
                test: /\.(jsx)$/,
                loader: 'esbuild-loader',
                options: {
                    loader: 'jsx',
                    target: 'es2015',
                },
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.(ts|tsx)?$/,
                loader: 'esbuild-loader',
                options: {
                    loader: 'tsx',
                    target: 'es2015',
                },
            },

            // CSS
            // ===
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },

            // SASS
            // ====
            {
                test: /\.sass$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                plugins: () => [autoprefixer],
                            },
                        },
                    },
                    {
                        loader: 'sass-loader',
                    },
                ],
            },
        ],
    },

    // игнорируем папку с модулями для скорости. Можно раскомментировать.
    watchOptions: { ignored: /node_modules/ },

    // метод сборки source-map. Sourcemaps включаются только в режиме development.
    devtool: sourceMaps,

    resolve: {
        modules: [nodePath, customNodePath, customNodePathBuild],
        extensions: ['.js', '.json', '.jsx', '.tsx', '.ts'],
        fallback: {
            'react/jsx-runtime': 'react/jsx-runtime.js',
            'react/jsx-dev-runtime': 'react/jsx-dev-runtime.js',
        },
    },

    plugins: [miniCss],

    resolveLoader: {
        modules: [nodePath],
    },
};

module.exports = config;
