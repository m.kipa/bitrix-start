# Установка

1. Параметры проекта
   Клонируем репозиторий
    ```bash
    git clone git@gitlab.com:webpractik/bitrix-start.git
   ```
   Клонируем файл окржуения .env
    ```bash
    dl env
    ```

2. Запускаем проект
    ```bash
    dl service up
    dl up
    ```

3. Скачиваем загрузчик bitrix
    ```bash
    wget https://www.1c-bitrix.ru/download/scripts/bitrixsetup.php
    ```
    
    После этого открываем файл /bitrixsetup.php в браузере

4. Установка bitrix
    
    #### Выбор продукта
    - Управление сайтом: редакция стандарт
    
    #### 3 Регистрация продукта
    - **Имя пользователя:** Вебпрактик
    - **Фамилия пользователя:** Битекович
    - **Email:** production@webpractik.ru
    - **Проект должен быть установлен в UTF8
   
   #### 4 перед созданием базы устанавливаем зависимости для битрикса
   ```bash
    dl bash
    composer install
    ```

    #### 5 Создание базы данных
    - **Сервер:** db
    - **Имя пользователя:** db
    - **Пароль:** db
    - **База данных:** существующая
    - **Имя базы данных:** db
    
    #### 6 Создание администратора
    - **Login:** webpractik
    - **Пароль:** стандартный (смотри мануал)
    
    #### 7 Выбор решения
    - Выбрать Демо-версия для разработчиков
    - После нажать отмена

5. Устанавливаем зависимости для фронта
    ```bash
    npm install
    ```

6. Переводим репозиторий на свой адрес гита
    ```bash
    git remote set-url origin SSHАдресРепозитория
    ```
   Теперь все коммиты будут идти в ваш репозиторий 
